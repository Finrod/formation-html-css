# Formation HTML/CSS

## Steps

  1. HTML structure
  2. Start styling with CSS
  3. Bootstrap CSS & Font Awesome
  4. Responsive classes and media queries
  5. Bootstrap Javascript components
  6. Preprocessing CSS with LESS

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Start Bootstrap Freelancer theme

All credits to [Start Bootstrap](https://startbootstrap.com/template-overviews/freelancer/)
